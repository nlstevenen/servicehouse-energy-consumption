**Energy Consumption assignment by Servicehouse** 

Not all requirements are implemented because of time restraints. I focussed mainly on the overall design of the application rather than the  correctness of the calculations at this time. Additionally, in this version I didn't take the time to optimise / refactor  the code since I see this application as a starting point to have a healthy discussion, opposed to a fully functioning micro service.

The code contains some comments to indicate some of the missing steps and ideas how to go about finishing the assignment.  

*-Steven Kok*