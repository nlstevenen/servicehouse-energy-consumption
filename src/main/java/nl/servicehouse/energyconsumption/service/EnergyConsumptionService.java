package nl.servicehouse.energyconsumption.service;

import lombok.AllArgsConstructor;
import nl.servicehouse.energyconsumption.controller.resource.EnergyConsumptionResource;
import nl.servicehouse.energyconsumption.meterreadings.EnergyConsumption;
import nl.servicehouse.energyconsumption.meterreadings.EnergyConsumptions;
import nl.servicehouse.energyconsumption.meterreadings.MeterReadingsReader;
import org.springframework.stereotype.Service;

import java.time.Month;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EnergyConsumptionService {
    private final MeterReadingsReader meterReadingsReader;

    /**
     * Consumption for a month, being that the difference between the meter reading of a month
     * minus the previous one, should be consistent with the fraction with a tolerance of a 25%. That
     * means that if the total consumption for a year is for example 240, if Fraction for February is 0.2
     * (meaning 20% of the consumption expected), then allowed values for Consumption in February
     * are 36-60. Note that these allowed values are for Consumption, not for Meter readings.
     */
    public EnergyConsumptionResource getConsumptions(String meterId, Month month) {
        EnergyConsumptions energyConsumptions = meterReadingsReader.getEnergyConsumptions();
        List<EnergyConsumption> consumptionsOfMeter = energyConsumptions.getEnergyConsumptions().stream()
                .filter(energyConsumption -> energyConsumption.getMeterId().equals(meterId))
                .collect(Collectors.toList());

        Optional<Integer> readingCurrentMonth = consumptionsOfMeter.stream()
                .filter(energyConsumption -> energyConsumption.getMonth().equals(month))
                .map(energyConsumption -> energyConsumption.getMeterReading())
                .findAny();

        //todo what to do when month is JANUARY?
        Optional<Integer> readingPreviousMonth = consumptionsOfMeter.stream()
                .filter(energyConsumption -> energyConsumption.getMonth().equals(month.minus(1)))
                .map(energyConsumption -> energyConsumption.getMeterReading())
                .findAny();

        //todo calculate consumption
        int consumption = 0;

        //todo: verify allowed fractions

        return new EnergyConsumptionResource(Month.JANUARY, consumption);
    }
}
