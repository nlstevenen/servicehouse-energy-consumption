package nl.servicehouse.energyconsumption.controller.resource;

import lombok.Data;

import java.time.Month;

@Data
public class EnergyConsumptionResource {
    private final Month month;
    private final int consumption;
}
