package nl.servicehouse.energyconsumption.controller;

import lombok.RequiredArgsConstructor;
import nl.servicehouse.energyconsumption.controller.resource.EnergyConsumptionResource;
import nl.servicehouse.energyconsumption.service.EnergyConsumptionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.Month;

@RestController
@RequiredArgsConstructor
public class EnergyConsumptionController {
    private final EnergyConsumptionService energyConsumptionService;

    /**
     * todo: possible to use Swagger as API documentation
     * todo: add javax.validation to validate input
     */
    @GetMapping(value = "energy-consumptions/meters/{meter}/months/{month}", produces = "application/json")
    public EnergyConsumptionResource retrieveConsumptions(@PathVariable String meter, @PathVariable Month month) {
        return energyConsumptionService.getConsumptions(meter, month);
    }
}
