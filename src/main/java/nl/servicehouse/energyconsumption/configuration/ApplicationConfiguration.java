package nl.servicehouse.energyconsumption.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import nl.servicehouse.energyconsumption.meterreadings.EnergyConsumption;
import nl.servicehouse.energyconsumption.parser.EnergyConsumptionDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public CsvMapper csvMapper() {
        CsvMapper csvMapper = new CsvMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addDeserializer(EnergyConsumption.class, new EnergyConsumptionDeserializer());
        csvMapper.registerModule(simpleModule);
        return csvMapper;
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper;
    }
}
