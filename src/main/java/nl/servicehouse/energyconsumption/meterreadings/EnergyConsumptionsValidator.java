package nl.servicehouse.energyconsumption.meterreadings;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class EnergyConsumptionsValidator {

    public List<EnergyConsumption> getValidEnergyConsumptions(List<EnergyConsumption> energyConsumptions) {
        // groupby meterId
        Map<String, List<EnergyConsumption>> energyConsumptionsByMeterId = energyConsumptions.stream()
                .collect(Collectors.groupingBy(EnergyConsumption::getMeterId));

        return energyConsumptionsByMeterId.keySet().stream()
                .map(key -> energyConsumptionsByMeterId.get(key))
                .map(e -> sortOnMonth(e))

                //If there is a validation error for a given meter data, only the data for that meter is being rejected.
                .filter(e -> validateReadingsCanOnlyIncrease(e))

                .flatMap(e -> e.stream())
                .collect(Collectors.toList());
    }

    //todo
    /**
     * Fractions for the profiles contained in the data should exist. If profiles A and B are mentioned,
     * there should be data in the database for profiles A and B.
     */
    protected void validateFractionsExist(List<EnergyConsumption> list) {
        // retrieve fractions from repository to verify profiles exist
    }

    /**
     * For a given meter a reading for a month should not be lower than the previous one.
     */
    protected boolean validateReadingsCanOnlyIncrease(List<EnergyConsumption> sorted) {
        try {
            sorted.stream()
                    .reduce((a, b) -> {
                        if (a.getMeterReading() <= b.getMeterReading()) {
                            return b;
                        } else {
                            throw new IllegalArgumentException("Reading is lower than previous month");
                        }
                    });
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    protected List<EnergyConsumption> sortOnMonth(List<EnergyConsumption> energyConsumptions) {
        return energyConsumptions.stream()
                .sorted() // sort on month
                .collect(Collectors.toList());
    }
}
