package nl.servicehouse.energyconsumption.meterreadings;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Month;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonPropertyOrder({"meterId", "profile", "month", "meterReading"})
public class EnergyConsumption implements Comparable<EnergyConsumption> {
    public String meterId;
    private String profile;
    private Month month;
    private int meterReading;

    @Override
    public int compareTo(EnergyConsumption o) {
        return this.month.compareTo(o.getMonth());
    }
}
