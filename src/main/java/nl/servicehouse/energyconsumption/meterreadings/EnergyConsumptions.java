package nl.servicehouse.energyconsumption.meterreadings;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class EnergyConsumptions {
    private final List<EnergyConsumption> energyConsumptions;
}
