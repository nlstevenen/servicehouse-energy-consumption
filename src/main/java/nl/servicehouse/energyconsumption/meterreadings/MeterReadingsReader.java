package nl.servicehouse.energyconsumption.meterreadings;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.servicehouse.energyconsumption.parser.EnergyConsumptionParser;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class MeterReadingsReader {

    private final CsvMapper csvMapper;
    private final EnergyConsumptionsValidator energyConsumptionsValidator;
    private final EnergyConsumptionParser energyConsumptionParser;

    public EnergyConsumptions getEnergyConsumptions() {
        try {
            List<EnergyConsumption> energyConsumptions = convert(energyConsumptionParser.parseFile("energyconsumptions.csv"));

            List<EnergyConsumption> validEnergyConsumptions = energyConsumptionsValidator.getValidEnergyConsumptions(energyConsumptions);

            return new EnergyConsumptions(validEnergyConsumptions);

        } catch (URISyntaxException | IOException e) {
            log.error("failed to read energy consumptions");

            return new EnergyConsumptions(Arrays.asList());
        }
    }

    protected List<EnergyConsumption> convert(String energyConsumptions) throws IOException {

        MappingIterator<EnergyConsumption> iterator = csvMapper
                .readerWithTypedSchemaFor(EnergyConsumption.class)
                .readValues(energyConsumptions);

        return iterator.readAll();
    }
}

