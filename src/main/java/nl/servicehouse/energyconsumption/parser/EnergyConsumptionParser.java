package nl.servicehouse.energyconsumption.parser;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;

@Component
public class EnergyConsumptionParser {
    public String parseFile(String resourceName) throws IOException, URISyntaxException {
        return new String(Files.readAllBytes(
                new File(Thread.currentThread()
                        .getContextClassLoader()
                        .getResource(resourceName) // Logic can be extracted to read from any kind of data source
                        .toURI()
                ).toPath()
        ));
    }
}
