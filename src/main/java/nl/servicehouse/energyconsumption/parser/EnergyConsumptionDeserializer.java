package nl.servicehouse.energyconsumption.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import nl.servicehouse.energyconsumption.meterreadings.EnergyConsumption;

import java.io.IOException;
import java.time.Month;

public class EnergyConsumptionDeserializer extends JsonDeserializer<EnergyConsumption> {

    @Override
    public EnergyConsumption deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode node = p.getCodec().readTree(p);
        String meterId = node.get("meterId").textValue();
        String month = node.get("month").textValue();
        String profile = node.get("profile").textValue();
        int meterReading = node.get("meterReading").intValue();

        return EnergyConsumption.builder()
                .meterId(meterId)
                .month(convert(month))
                .profile(profile)
                .meterReading(meterReading)
                .build();
    }

    /**
     * To be able to use Java.time.Month, we need this converter
     */
    // todo add unit tests
    private Month convert(String month) {

        switch (month) {
            case ("JAN"):
                return Month.JANUARY;
            case ("FEB"):
                return Month.FEBRUARY;
            case ("MAR"):
                return Month.MARCH;
            case ("APR"):
                return Month.APRIL;
            case ("MAY"):
                return Month.MAY;
            case ("JUN"):
                return Month.JUNE;
            case ("JUL"):
                return Month.JULY;
            case ("AUG"):
                return Month.AUGUST;
            case ("SEP"):
                return Month.SEPTEMBER;
            case ("OKT"):
                return Month.OCTOBER;
            case ("NOV"):
                return Month.NOVEMBER;
            case ("DEC"):
                return Month.DECEMBER;
            default:
                throw new IllegalArgumentException("unknown/invalid input for: " + month);
        }
    }
}
