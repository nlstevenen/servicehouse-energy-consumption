package nl.servicehouse.energyconsumption.integration;

import com.jayway.jsonpath.JsonPath;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class EnergyConsumptionControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void retrieveConsumptionOfJanuary() {
        ResponseEntity<String> result = restTemplate.getForEntity("/energy-consumptions/meters/0001/months/JANUARY", String.class);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

        String month = JsonPath.parse(result.getBody()).read("$.month");
        assertThat(month).isEqualTo("JANUARY");
    }


}