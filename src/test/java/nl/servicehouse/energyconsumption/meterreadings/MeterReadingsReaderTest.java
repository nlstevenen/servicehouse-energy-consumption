package nl.servicehouse.energyconsumption.meterreadings;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import nl.servicehouse.energyconsumption.parser.EnergyConsumptionParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Month;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class MeterReadingsReaderTest {

    private MeterReadingsReader meterReadingsReader;
    @Mock
    private EnergyConsumptionsValidator energyConsumptionsValidator;
    private EnergyConsumptionParser energyConsumptionParser;

    @Before
    public void setup() {
        energyConsumptionParser = new EnergyConsumptionParser();
        meterReadingsReader = new MeterReadingsReader(new CsvMapper(), energyConsumptionsValidator,energyConsumptionParser);
    }

    @Test
    public void readEnergyConsumptions() throws IOException, URISyntaxException {
        energyConsumptionParser = new EnergyConsumptionParser();
        List<EnergyConsumption> energyConsumptions = meterReadingsReader.convert(this.energyConsumptionParser.parseFile("2energyconsumptions.csv"));
        assertThat(energyConsumptions).hasSize(2);

        assertThat(energyConsumptions.get(0).getMeterId()).isEqualTo("0001");
        assertThat(energyConsumptions.get(0).getProfile()).isEqualTo("A");
        assertThat(energyConsumptions.get(0).getMonth()).isEqualTo(Month.JANUARY);
        assertThat(energyConsumptions.get(0).getMeterReading()).isEqualTo(10);

        assertThat(energyConsumptions.get(1).getMeterId()).isEqualTo("0004");
        assertThat(energyConsumptions.get(1).getProfile()).isEqualTo("B");
        assertThat(energyConsumptions.get(1).getMonth()).isEqualTo(Month.JANUARY);
        assertThat(energyConsumptions.get(1).getMeterReading()).isEqualTo(8);
    }
}