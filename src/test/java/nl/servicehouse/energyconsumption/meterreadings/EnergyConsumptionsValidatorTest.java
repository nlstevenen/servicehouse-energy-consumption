package nl.servicehouse.energyconsumption.meterreadings;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class EnergyConsumptionsValidatorTest {

    @InjectMocks
    private EnergyConsumptionsValidator energyConsumptionsValidator;

    @Test
    public void shouldBeSortedOnMonth() {
        List<EnergyConsumption> energyConsumptions = energyConsumptionsValidator.sortOnMonth(Arrays.asList(
                createEnergyConsumption(Month.FEBRUARY),
                createEnergyConsumption(Month.DECEMBER),
                createEnergyConsumption(Month.JANUARY),
                createEnergyConsumption(Month.APRIL)
        ));

        assertThat(energyConsumptions).hasSize(4);
        assertThat(energyConsumptions.get(0).getMonth()).isEqualTo(Month.JANUARY);
        assertThat(energyConsumptions.get(1).getMonth()).isEqualTo(Month.FEBRUARY);
        assertThat(energyConsumptions.get(2).getMonth()).isEqualTo(Month.APRIL);
        assertThat(energyConsumptions.get(3).getMonth()).isEqualTo(Month.DECEMBER);

    }

    private EnergyConsumption createEnergyConsumption(Month month) {
        return EnergyConsumption.builder()
                .month(month)
                .build();
    }

    private EnergyConsumption createEnergyConsumption(int meterReading) {
        return EnergyConsumption.builder()
                .meterReading(meterReading)
                .build();
    }

    private EnergyConsumption createEnergyConsumption(Month month, int meterReading, String meterId) {
        return EnergyConsumption.builder()
                .meterReading(meterReading)
                .month(month)
                .meterId(meterId)
                .build();
    }

    public void readingIsLowerThanLastMonth() {
        boolean result = energyConsumptionsValidator.validateReadingsCanOnlyIncrease(Arrays.asList(
                createEnergyConsumption(2),
                createEnergyConsumption(1)
        ));
        assertThat(result).isFalse();
    }

    @Test
    public void validReading() {
        energyConsumptionsValidator.validateReadingsCanOnlyIncrease(Arrays.asList(
                createEnergyConsumption(1),
                createEnergyConsumption(2),
                createEnergyConsumption(3),
                createEnergyConsumption(4)
        ));
    }

    @Test
    public void dontReturnMeterWithInvalidReadings() {
        List<EnergyConsumption> validEnergyConsumptions = energyConsumptionsValidator.getValidEnergyConsumptions(Arrays.asList(
                createEnergyConsumption(Month.JANUARY, 2, "A"),
                createEnergyConsumption(Month.FEBRUARY, 1, "A")
                )
        );

        assertThat(validEnergyConsumptions).isEmpty();
    }

    @Test
    public void returnMeterWithValidReadings() {
        List<EnergyConsumption> validEnergyConsumptions = energyConsumptionsValidator.getValidEnergyConsumptions(Arrays.asList(
                createEnergyConsumption(Month.JANUARY, 1, "A"),
                createEnergyConsumption(Month.FEBRUARY, 2, "A")
                )
        );

        assertThat(validEnergyConsumptions).hasSize(2);
    }

    @Test
    public void returnOnlyMeterWithValidReadings() {
        List<EnergyConsumption> validEnergyConsumptions = energyConsumptionsValidator.getValidEnergyConsumptions(Arrays.asList(
                createEnergyConsumption(Month.JANUARY, 1, "A"),
                createEnergyConsumption(Month.FEBRUARY, 2, "A"),
                createEnergyConsumption(Month.JANUARY, 2, "B"),
                createEnergyConsumption(Month.FEBRUARY, 1, "B")
                )
        );

        assertThat(validEnergyConsumptions).hasSize(2);
        assertThat(validEnergyConsumptions.get(0).getMeterId()).isEqualTo("A");
        assertThat(validEnergyConsumptions.get(1).getMeterId()).isEqualTo("A");
    }


}